
# _______________________________Thêm thư viện___________________________________________________________

from flask import Flask, render_template, Response
from queue import Queue
import cv2
import numpy as np
import threading
import time
import base64
from flask_socketio import SocketIO,emit
import imagezmq
#import subprocess
from flask_sqlalchemy import SQLAlchemy

# _______________________________Khai báo__________________________________________________________________

app = Flask(__name__)
#Stream
socketio = SocketIO(app,async_mode='threading')
countConnect = 0
imageRecv = imagezmq.ImageHub(open_port = 'tcp://127.0.0.1:1998',REQ_REP= False)
#Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///video.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
class Video(db.Model):
    ten = db.Column(db.String(50), primary_key=True)
    thoigian = db.Column(db.String(20), nullable=False)
#db.create_all()
# ______________________________API_______________________________________________________________________
#App
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/api/streaming')
def streaming():
    return render_template('videostream.html')

@app.route('/api/videoplayback')
def videoplayback():
	videos = Video.query.all()
	return render_template('videoplayback.html',videos=videos)

#SocketIO

#Nếu có client connect
@socketio.on('connect')
def connectChannel():
    global countConnect
    countConnect+=1
#Nếu client disconnect
@socketio.on('disconnect')
def disconnectChannel():
    global countConnect
    if countConnect >0:
        countConnect-=1
    elif countConnect<0:
        countConnect=0

# _______________________________Funtion_________________________________________________________________

def thSendImg():
    global countConnect
    while True:
    	#nhận hình ảnh từ Yolo
    	msg,img = imageRecv.recv_image()
    	#encode ảnh
    	_,img_encode = cv2.imencode('.jpg',img)
    	#encode base64
    	img_64 = base64.b64encode(img_encode).decode('ascii')
    	#Nếu có client connect thì gửi ảnh
    	if countConnect>0 :
    		socketio.emit('imgChannel', {'data': img_64}, broadcast=True)
# _______________________________Main()__________________________________________________________________

if __name__ == '__main__':
	threading.Thread(target=thSendImg, daemon=True).start()
	#subprocess.Popen("python yolov4.py", shell = True)
	#subprocess.Popen("python control.py", shell = True)
	socketio.run(app,host='0.0.0.0',port=5000)


