#________________________________________Import____________________________________________

import imagezmq
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2
import numpy as np
#_______________________________________________________________________________________
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 10
rawCapture = PiRGBArray(camera, size=(640, 480))
imageSender = imagezmq.ImageSender(connect_to = 'tcp://169.254.99.118:3021',REQ_REP = False)

#________________________________________Main()_______________________________________________

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	image = frame.array
	imageSender.send_image('client',image)
	rawCapture.truncate(0)